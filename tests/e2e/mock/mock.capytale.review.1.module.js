// MGU 01/03/ 2025 attention à laisser en JS.
// C'est un fichier qui est injecté directement dans le navigateur de playwright pour similer le module capytale.

// eslint-disable-next-line import-x/no-absolute-path
import * as RPCm from '/alea/node_modules/.vite/deps/@mixer_postmessage-rpc.js'

const RPC = RPCm.default.RPC

const serviceId = 'capytale-player'

const myIframe = document.querySelector('iframe')
const rpc = new RPC({
  // The window you want to talk to:
  target: myIframe.contentWindow,
  // This should be unique for each of your producer<->consumer pairs:
  serviceId,

  // Optionally, allowlist the origin you want to talk to:
  origin: '*'
})

const activityParams = {
  mode: 'review',
  workflow: 'current',
  activity: {
    exercicesParams: [
      {
        uuid: '51242',
        id: 'canc3D04',
        interactif: '1',
        alea: 'stG6'
      },
      {
        uuid: '6225c',
        id: '6M23',
        interactif: '1',
        alea: 'Qlfa'
      },
      {
        uuid: '6225c',
        id: '6M23',
        interactif: '1',
        nbQuestions: 1,
        duration: 10,
        sup: '3',
        sup2: 'false',
        sup3: '2',
        sup4: 'false',
        cd: '1',
        alea: '4JMh'
      },
      {
        uuid: 'f8dee',
        id: '6G10-8',
        interactif: '1',
        alea: 'XEP0'
      },
      {
        uuid: '83763',
        id: '6G10-3',
        interactif: '1',
        nbQuestions: 2,
        duration: 10,
        cd: '1',
        alea: 'AJha'
      },
      {
        uuid: 'e6f62',
        id: '6C13-2',
        interactif: '1',
        alea: 'fdfj'
      },
      {
        uuid: '0688e',
        id: '6N10',
        interactif: '1',
        nbQuestions: 1,
        duration: 10,
        sup: '4',
        sup2: '0',
        sup3: '1',
        sup4: 'true',
        cd: '1',
        alea: 'gFXm'
      }
    ],
    globalOptions: {
      v: '',
      z: '1',
      durationGlobal: 0,
      nbVues: 1,
      flow: 0,
      isImagesOnSides: false,
      sound: 0,
      shuffle: false,
      select: [],
      order: [],
      title: '',
      presMode: 'un_exo_par_page',
      setInteractive: '2',
      isSolutionAccessible: true,
      isInteractiveFree: true,
      oneShot: false,
      twoColumns: false,
      isTitleDisplayed: true,
      recorder: 'capytale',
      beta: false,
      iframe: '',
      answers: ''
    },
    canOptions: {
      durationInMinutes: 540,
      subTitle: '',
      isChoosen: false,
      solutionsAccess: true,
      solutionsMode: 'gathered',
      isInteractive: true,
      remainingTimeInSeconds: 0,
      questionGetAnswer: [],
      state: 'start'
    }
  },
  studentAssignment: [
    {
      uuid: '51242',
      title: 'Indiquer l\'heure sur une horloge',
      indice: 0,
      state: 'done',
      alea: 'stG6',
      answers: {
        clockEx0Q0: '8h10'
      },
      numberOfPoints: 1,
      numberOfQuestions: 1,
      bestScore: 1
    },
    {
      uuid: '6225c',
      title: 'Convertir des aires',
      indice: 1,
      state: 'done',
      alea: 'Qlfa',
      answers: {
        Ex1Q0R0: '0',
        Ex1Q0R1: '0',
        Ex1Q0R2: '0',
        Ex1Q0R3: '1',
        Ex1Q0R4: '0'
      },
      numberOfPoints: 1,
      numberOfQuestions: 1,
      bestScore: 1
    },
    {
      uuid: '6225c',
      title: 'Convertir des aires',
      indice: 2,
      state: 'done',
      alea: '4JMh',
      answers: {
        Ex2Q0: '600000'
      },
      numberOfPoints: 1,
      numberOfQuestions: 1,
      bestScore: 1
    },
    {
      uuid: 'f8dee',
      title: 'Utiliser la définition du cercle et du disque',
      indice: 3,
      state: 'done',
      alea: 'XEP0',
      answers: {
        apigeomEx3F06GXX0: JSON.stringify({
          apiGeomVersion: '3.0.20230508',
          options: {
            animationStepInterval: 3000,
            automaticUserMessage: true,
            borderSize: 0.2,
            color: 'red',
            colorPointPolygon: 'none',
            changeColorChangeActionToSetOptions: true,
            discFillOpacity: 0.2,
            displayGrid: false,
            distanceWithoutNewPoint: 0.2,
            fillColor: 'none',
            fillColorAndBorderColorAreSame: true,
            fillOpacity: 0.2,
            gridWithTwoPointsOnSamePosition: true,
            fontSize: '1em',
            isDashed: false,
            labelDxInPixels: 15,
            labelDyInPixels: 15,
            latexHeight: 12,
            labelIsVisible: true,
            latexWidth: 18,
            limitNumberOfElement: {},
            mark: '||',
            moveTextGrid: 15,
            pointDescriptionWithCoordinates: true,
            pointSize: 5,
            thickness: 1,
            shape: 'x',
            shapeForPolygon: 'x',
            thicknessForPoint: 2,
            tmpColor: 'gray',
            tmpFillColor: 'rgba(241, 89, 41, 0.5)',
            tmpFillOpacity: 0.2,
            tmpIsDashed: true,
            tmpThickness: 1,
            tmpShape: 'x'
          },
          point1: {
            color: 'currentColor',
            id: 'point1',
            isDashed: false,
            isVisible: true,
            isSelectable: true,
            isDeletable: false,
            opacity: 1,
            thickness: 2,
            type: 'Point',
            colorLabel: 'currentColor',
            label: 'B',
            labelDxInPixels: 10,
            labelDyInPixels: 20,
            shape: 'x',
            sizeInPixels: 5,
            x: 1,
            y: 2
          },
          point2: {
            color: 'currentColor',
            id: 'point2',
            isDashed: false,
            isVisible: true,
            isSelectable: true,
            isDeletable: false,
            opacity: 1,
            thickness: 2,
            type: 'Point',
            colorLabel: 'currentColor',
            label: 'S',
            labelDxInPixels: 10,
            labelDyInPixels: 20,
            shape: 'x',
            sizeInPixels: 5,
            x: -3,
            y: 2
          },
          element0: {
            color: 'blue',
            id: 'element0',
            isDashed: false,
            isVisible: true,
            isSelectable: true,
            isDeletable: true,
            opacity: 1,
            thickness: 1,
            type: 'Circle',
            fillColor: 'none',
            fillOpacity: 0.2,
            idCenter: 'point1',
            radius: '3.7'
          },
          element1: {
            color: 'red',
            id: 'element1',
            isDashed: false,
            isVisible: true,
            isSelectable: true,
            isDeletable: true,
            opacity: 1,
            thickness: 1,
            type: 'Circle',
            fillColor: 'red',
            fillOpacity: 0.2,
            idCenter: 'point2',
            radius: '5'
          }
        })
      },
      numberOfPoints: 2,
      numberOfQuestions: 2,
      bestScore: 2
    },
    {
      uuid: '83763',
      title: 'Choisir la bonne figure',
      indice: 4,
      state: 'done',
      alea: 'AJha',
      answers: {
        cliquefigure0Ex4Q0: '1',
        cliquefigure3Ex4Q1: '1'
      },
      numberOfPoints: 2,
      numberOfQuestions: 2,
      bestScore: 2
    },
    {
      uuid: 'e6f62',
      title: 'Traduire une expression par une phrase',
      indice: 5,
      state: 'done',
      alea: 'fdfj',
      answers: {
        ex5Q0: 'produit',
        ex5Q1: 'somme',
        ex5Q2: 'différence',
        ex5Q3: 'quotient'
      },
      numberOfPoints: 4,
      numberOfQuestions: 4,
      bestScore: 4
    },
    {
      uuid: '0688e',
      title: 'Écrire un nombre entier en chiffres ou en lettres',
      indice: 6,
      state: 'done',
      alea: 'gFXm',
      answers: {
        rectangleDNDEx6Q0R1: 'etiquetteEx6Q0I23-clone-1740911812656;etiquetteEx6Q0I12-clone-1740911816311;etiquetteEx6Q0I7-clone-1740911820479;etiquetteEx6Q0I12-clone-1740911825887;etiquetteEx6Q0I22-clone-1740911829543;etiquetteEx6Q0I12-clone-1740911834455;etiquetteEx6Q0I41-clone-1740911838551;etiquetteEx6Q0I12-clone-1740911841007;etiquetteEx6Q0I37-clone-1740911842831;etiquetteEx6Q0I12-clone-1740911845127;etiquetteEx6Q0I21-clone-1740911849720'
      },
      numberOfPoints: 1,
      numberOfQuestions: 1,
      bestScore: 1
    }
  ]
}

rpc.expose('toolGetActivityParams', () => activityParams)

rpc.expose('hasChanged', () => console.log('hasChanged'))

rpc.expose('saveStudentAssignment', (data) => {
  console.log('saveStudentAssignment', data)
  window.localStorage.setItem('saveStudentAssignment', JSON.stringify(data))
  return Promise.resolve()
})
