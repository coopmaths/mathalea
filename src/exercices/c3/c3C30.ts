import MultiplierDecimauxPar101001000 from '../6e/6C30-1'
export const titre = 'Multiplier un nombre décimal par 10, 100 ou 1 000'
export const amcReady = true
export const amcType = 'AMCNum'
export const interactifReady = true
export const interactifType = 'mathLive'

export const dateDeModifImportante = '11/02/2025'

/**
 *
 * @author Jean-Claude Lhote et Rémi Angot
 */
export const uuid = '7fa0e'

export const refs = {
  'fr-fr': ['c3C30'],
  'fr-ch': []
}
export default class MultiplierDecimauxPar101001000C3 extends MultiplierDecimauxPar101001000 {
  constructor () {
    super()
    this.nbQuestions = 3
    this.sup = '1'
    this.sup2 = false
  }
}
