import ÉcrireNombresEntiers from '../6e/6N10'
export const titre = 'Écrire un nombre en chiffres ou en lettres'
export const amcReady = true
export const amcType = 'AMCOpen'
export const interactifReady = true
export const interactifType = 'mathLive'
export const dateDeModifImportante = '14/11/2023'
export const uuid = '85618'
export const refs = {
  'fr-fr': ['c3N10'],
  'fr-ch': []
}
export default class ÉcrireEntiersCycle3 extends ÉcrireNombresEntiers {
  constructor () {
    super()
    this.sup2 = 0
    this.sup = 1
  }
}
