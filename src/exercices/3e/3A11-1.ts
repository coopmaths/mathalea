import ProblemesEvenementsRecurrents from '../4e/4A12'
export const titre = 'Résoudre des problèmes de conjonction de phénomènes'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'
export const dateDePublication = '30/10/2021'
export const uuid = '80772'
export const refs = {
  'fr-fr': ['3A11-1'],
  'fr-ch': []
}
export default class ProblemesEvenementsRecurrents3e extends ProblemesEvenementsRecurrents {
  constructor () {
    super()
    this.sup = 2
  }
}
