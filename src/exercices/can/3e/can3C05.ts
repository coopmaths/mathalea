import { rienSi1 } from '../../../lib/outils/ecritures'
import { texNombre } from '../../../lib/outils/texNombre'
import Exercice from '../../Exercice'
import { randint, calculANePlusJamaisUtiliser } from '../../../modules/outils'
export const titre = 'Calculer avec une puissance de 10'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCNum'
/**
 * Modèle d'exercice très simple pour la course aux nombres
 * @author Gilles Mora
 * Créé pendant l'été 2021

 * Date de publication
*/
export const uuid = 'ce089'

export const refs = {
  'fr-fr': ['can3C05'],
  'fr-ch': []
}
export default class CalculPuissance10 extends Exercice {
  constructor () {
    super()

    this.typeExercice = 'simple'
    this.nbQuestions = 1
  }

  nouvelleVersion () {
    const a = randint(1, 6)
    const n = calculANePlusJamaisUtiliser(2 * randint(1, 6) + 1) / 2
    const N = calculANePlusJamaisUtiliser(2 * randint(1, 6, a) + 1) / 2
    const c = randint(1, 3)
    const d = randint(1, 3)
    this.question = `Calculer sous forme décimale $B=${texNombre(n)}\\times 10^{${rienSi1(c)}}+${texNombre(N)}\\times 10^{${rienSi1(d)}}$.`
    this.correction = `$B=${texNombre(n)}\\times 10^{${rienSi1(c)}}+${texNombre(N)}\\times 10^{${rienSi1(d)}}=${texNombre(n * 10 ** c)}+${texNombre(N * 10 ** d)}=${texNombre(n * 10 ** c + N * 10 ** d)}$.`
    this.reponse = calculANePlusJamaisUtiliser(n * 10 ** c + N * 10 ** d)
    this.canEnonce = this.question// 'Compléter'
    this.canReponseACompleter = ''
  }
}
