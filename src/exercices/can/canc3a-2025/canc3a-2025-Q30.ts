import Exercice from '../../Exercice'
import { miseEnEvidence } from '../../../lib/outils/embellissements'
import { randint } from '../../../modules/outils'
import { KeyboardType } from '../../../lib/interactif/claviers/keyboard'
export const titre = 'Calculer le résultat d\'une division par 5'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = '845a2'
export const refs = {
  'fr-fr': [],
  'fr-ch': []
}
/**
 * Modèle d'exercice très simple pour la course aux nombres
 * @author Gilles Mora
*/
export default class Can2025CM2Q30 extends Exercice {
  constructor () {
    super()

    this.typeExercice = 'simple'
    this.nbQuestions = 1
    this.formatChampTexte = KeyboardType.clavierDeBase
    this.optionsChampTexte = { texteAvant: '$=$' }
  }

  nouvelleVersion () {
    const b = this.canOfficielle ? 12 : randint(13, 19)

    this.question = ` $${b * 5} \\div 5$`
    this.correction = `$${b * 5} \\div 5 =${miseEnEvidence(b)}$.`

    this.reponse = b
    this.canEnonce = this.question
    this.canReponseACompleter = ''
  }
}
