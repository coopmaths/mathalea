import CalculerUnPourcentage from '../../6e/6N33-2'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcType = 'AMCNum'
export const amcReady = true
export const titre = 'Résoudre un problème de calcul de pourcentage par complément à 100%'

export const dateDePublication = '15/11/2022'

/**
 * @author Gilles Mora
 * Créé le 20/01/2022

 */

export const uuid = 'fb422'

export const refs = {
  'fr-fr': ['can6P06'],
  'fr-ch': []
}
export default class CalculerUnPourcentageCAN extends CalculerUnPourcentage {
  constructor () {
    super()
    this.nbQuestions = 1
    this.can = true
    this.consigne = ''
  }
}
