import { KeyboardType } from '../../../lib/interactif/claviers/keyboard'
import { choice } from '../../../lib/outils/arrayOutils'
import { miseEnEvidence, texteEnCouleur } from '../../../lib/outils/embellissements'
import { texNombre } from '../../../lib/outils/texNombre'
import { randint } from '../../../modules/outils'
import Decimal from 'decimal.js'
import { bleuMathalea } from '../../../lib/colors'
import Exercice from '../../Exercice'
export const titre = 'Calculer le double ou le triple (décimal)'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCNum'

/**
 * @author Jean-Claude Lhote
 * Créé pendant l'été 2021
 */
export const uuid = '50fc4'

export const refs = {
  'fr-fr': ['can6C11'],
  'fr-ch': []
}
export default class DoubleOuTripleDecimal extends Exercice {
  constructor () {
    super()
    this.typeExercice = 'simple'
    this.nbQuestions = 1

    this.formatChampTexte = KeyboardType.clavierNumbers
  }

  nouvelleVersion () {
    const a = randint(1, 3)
    const b = randint(1, 9, a)
    const e = a * 10 + b
    const d = (new Decimal(randint(1, 9))).div(10)
    const c = d.add(e)
    if (choice([true, false])) {
      this.reponse = c.mul(3)
      this.question = `Quel est le triple de $${texNombre(c, 1)}$ ?`
      this.correction = `Le triple de $${texNombre(c, 1)}$ est $3 \\times ${texNombre(c, 1)}=${miseEnEvidence(texNombre(Number(this.reponse), 1))}$.<br>`
      this.correction += texteEnCouleur(`
      <br> Mentalement : <br>
  On décompose $${texNombre(c, 1)}$ en $${e}+${texNombre(d, 1)}$. <br>
  On calcule le triple de $${e}$, soit $3\\times ${e}= ${3 * e}$
  puis le triple de $${texNombre(d, 1)}$, soit $3\\times ${texNombre(d, 1)}=${texNombre(d.mul(3), 1)}$.<br>
  On en fait la somme : $${3 * e}+${texNombre(d.mul(3), 1)}$, ce qui donne le résultat $${texNombre(Number(this.reponse), 1)}$.
      `, bleuMathalea)
    } else {
      this.reponse = c.mul(2)
      this.question = `Quel est le double de $${texNombre(c, 1)}$ ?`
      this.correction = `Le double de $${texNombre(c, 1)}$ est $2 \\times ${texNombre(c, 1)}=${miseEnEvidence(texNombre(Number(this.reponse), 1))}$.<br>`
      this.correction += texteEnCouleur(`
      <br> Mentalement : <br>
  On décompose $${texNombre(c, 1)}$ en $${e}+${texNombre(d, 1)}$. <br>
  On calcule le double de $${e}$, soit $2\\times ${e}= ${2 * e}$
  puis le double de $${texNombre(d, 1)}$, soit $2\\times ${texNombre(d, 1)}=${texNombre(d.mul(2), 1)}$.<br>
  On en fait la somme : $${2 * e}+${texNombre(d.mul(2), 1)}$, ce qui donne le résultat $${texNombre(Number(this.reponse), 1)}$.
      `, bleuMathalea)
    }
    this.canEnonce = this.question
    this.canReponseACompleter = ''
  }
}
