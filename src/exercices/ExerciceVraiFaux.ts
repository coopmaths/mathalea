import { propositionsQcm } from '../lib/interactif/qcm'
import { texteEnCouleurEtGras } from '../lib/outils/embellissements'
import { context } from '../modules/context'
import { listeQuestionsToContenu } from '../modules/outils'
import Exercice from './Exercice'

interface Affirmation {
  texte: string;
  statut: boolean;
  correction: string;
}

/*
 * @author Rémi Angot
 * Exercice vrai ou faux
 * Une sélection aléatoire d'affirmations est proposée à l'élève qui doit dire si elles sont vraies ou fausses
 * Ces affirmations sont tirées de this.affirmations
*/
export default class VraiFaux extends Exercice {
  affirmations: Affirmation[]
  constructor () {
    super()
    this.nbQuestions = 1
    this.consigne = 'Pour chaque affirmation, dire si elle est vraie ou fausse.'
    this.affirmations = []
    this.besoinFormulaireCaseACocher = ['Ajout de « Je ne sais pas »', false]
    this.sup = false
  }

  nouvelleVersion () {
    this.affirmations.sort(() => Math.random() - 0.5)
    this.nbQuestions = Math.min(this.affirmations.length, this.nbQuestions)
    for (let i = 0, cpt = 0; i < this.nbQuestions && cpt < 50;) {
      let texte = this.affirmations[i].texte
      const propositions = [
        {
          texte: 'Vrai',
          statut: this.affirmations[i].statut
        },
        {
          texte: 'Faux',
          statut: !this.affirmations[i].statut
        }
      ]
      if (this.sup) {
        propositions.push({
          texte: 'Je ne sais pas',
          statut: false
        })
      }
      this.autoCorrection[i] = {
        options: { ordered: true, vertical: false },
        enonce: texte,
        propositions
      }
      const monQcm = propositionsQcm(this, i)
      if (!context.isAmc) {
        texte += monQcm.texte
      }
      let correction = `L'affirmation est ${texteEnCouleurEtGras(this.affirmations[i].statut ? 'vraie' : 'fausse')}.<br>`
      correction += this.affirmations[i].correction

      if (this.questionJamaisPosee(i, this.affirmations[i].texte)) {
        this.listeQuestions[i] = texte
        this.listeCorrections[i] = correction
        i++
      }
      cpt++
    }
    listeQuestionsToContenu(this)
  }
}
