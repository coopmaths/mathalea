import ConstruireUnTriangleAvecCible from './_Construire_un_triangle_avec_cible'
export const titre = 'Construire un triangle'

export const interactifReady = false

export const uuid = 'a07bb'

export const refs = {
  'fr-fr': ['6G21-1'],
  'fr-ch': ['9ES4-8']
}
export default class ConstruireUnTriangleAvecCible6e extends ConstruireUnTriangleAvecCible {
  constructor () {
    super()
    this.classe = 6
  }
}
