import ProprietesParallelesPerpendiculaires from './6G52-2'

export const amcReady = true
export const amcType = 'AMCOpen'
export const titre = 'Utiliser les propriétés des droites parallèles et perpendiculaires'

/**
 * Exercice déréférencé au profit de 6G52-2 qui est rangé dans la bonne catégorie
 * @author Guillaume Valmont
 */
export const uuid = '6a336'
// J'ajoute des réfs vide pour éviter le message rouge dans le updateMenuInternationnal (au pnpm start, ça fait peur)
export const refs = {
  'fr-fr': [],
  'fr-ch': []
}
export default class ProprietesParallelesPerpendiculaires2 extends ProprietesParallelesPerpendiculaires {
}
