import CalculerLaValeurDUneExpressionLitterale from './5L14'
export const titre = 'Calculer la valeur d\'une expression littérale de degré 1 à 1 inconnue'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = '72764'
export const refs = {
  'fr-fr': ['5L14-5'],
  'fr-ch': ['9FA2-7']
}
export default class CalculerLaValeurDUneExpressionLitteraleDeg1Inc1 extends CalculerLaValeurDUneExpressionLitterale {
  constructor () {
    super()
    this.version = '5L13-5'
    this.nbQuestions = 2
  }
}
