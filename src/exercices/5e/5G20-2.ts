import TracerTriangle2Angles from '../6e/6G23-2'
export const titre = 'Tracer un triangle dont on connaît une longueur et 2 angles'
export const interactifReady = false
export const uuid = '6a1a2'
export const refs = {
  'fr-fr': ['5G20-2'],
  'fr-ch': []
}
export default class TracerTriangle2Angles5e extends TracerTriangle2Angles {
}
