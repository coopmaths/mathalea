import OrganiserDonneesDepuisTexte from '../6e/6S11'
export const titre = 'Organiser des données dans un tableau'
export const interactifReady = false
export const uuid = '60eb8'
export const refs = {
  'fr-fr': ['5S10'],
  'fr-ch': []
}
export default class OrganiserDonneesDepuisTexte5e extends OrganiserDonneesDepuisTexte {
  constructor () {
    super()
    this.sup = false
    this.sup2 = 4
  }
}
