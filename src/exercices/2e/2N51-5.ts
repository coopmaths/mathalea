import ExerciceEquation1Tiret2 from '../3e/3L13-1'
export const titre = 'Résoudre une équation du premier degré (utilisant la distributivité)'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = '3b3d9'
export const refs = {
  'fr-fr': ['2N51-5'],
  'fr-ch': []
}
export default class ExerciceEquation1Tiret22nde extends ExerciceEquation1Tiret2 {
}
