import ExerciceAdditionnerOuSoustraireDesFractions from '../4e/4C21'
export const titre = 'Additionner ou soustraire deux fractions'
export const amcReady = true
export const amcType = 'AMCNum'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = 'b51ec'
export const refs = {
  'fr-fr': ['2N30-2'],
  'fr-ch': []
}
export default class ExerciceAdditionnerOuSoustraireDesFractions2nde extends ExerciceAdditionnerOuSoustraireDesFractions {
}
