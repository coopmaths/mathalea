import FactoriserUneExpression3e from '../3e/3L11-6'
export const titre = 'Factoriser une expression complexe'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = '3d2f9'
export const refs = {
  'fr-fr': ['2N41-1'],
  'fr-ch': []
}
export default class FactoriserUneExpression3e2nde extends FactoriserUneExpression3e {
}
