import CalculerLaValeurDUneExpressionLitterale from '../5e/5L14'
export const titre = 'Calculer la valeur d\'une expression littérale'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = '98658'

export const refs = {
  'fr-fr': ['2N40-2'],
  'fr-ch': []
}
export default class CalculerLaValeurDUneExpressionLitterale2e extends CalculerLaValeurDUneExpressionLitterale {
}
