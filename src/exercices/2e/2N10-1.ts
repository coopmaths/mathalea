import LireAbscisseDecimaleBis2d from '../6e/6N30-1'
export const titre = 'Lire l\'abscisse décimale d\'un point'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCOpen'
export const dateDeModifImportante = '27/10/2021'
export const uuid = '507cf'
export const refs = {
  'fr-fr': ['2N10-1'],
  'fr-ch': []
}
export default class LireAbscisseDecimale2nde extends LireAbscisseDecimaleBis2d {
  constructor () {
    super()
    this.niveau = 2
  }
}
