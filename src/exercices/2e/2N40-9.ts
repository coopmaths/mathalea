import FactoriserParNombreOux from '../3e/3L11-4'
export const titre = 'Factoriser une expression'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCOpen'
export const uuid = '2e5df'
export const refs = {
  'fr-fr': ['2N40-9'],
  'fr-ch': []
}
export default class FactoriserParNombreOux2e extends FactoriserParNombreOux {
}
