import identitesCalculs from '../3e/3L11-5'
export const interactifReady = true
export const interactifType = 'mathLive'
export const titre = 'Calcul mental et calcul littéral'
export const uuid = '74c5a'
export const refs = {
  'fr-fr': ['2N40-8'],
  'fr-ch': []
}
export default class IdentitesCalculs2e extends identitesCalculs {
  constructor () {
    super()
    this.sup = 4
  }
}
