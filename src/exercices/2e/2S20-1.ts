import calculEffectifFrequence from '../3e/3S12'
export const titre = 'Calculer des effectifs et des fréquences'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcReady = true
export const amcType = 'AMCHybride'
export const uuid = 'dc3d2'
export const refs = {
  'fr-fr': ['2S20-1'],
  'fr-ch': []
}
export default class CalculEffectifFrequence2nde extends calculEffectifFrequence {
}
