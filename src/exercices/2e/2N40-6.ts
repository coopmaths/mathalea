import DistributiviteSimpleDoubleReduction from '../3e/3L11-3'
export const titre = 'Utiliser la distributivité (simple ou double) et réduire'
export const interactifReady = true
export const interactifType = 'mathLive'
export const amcType = 'AMCHybride'
export const amcReady = true
export const uuid = '60cc5'
export const refs = {
  'fr-fr': ['2N40-6'],
  'fr-ch': ['11FA2-7']
}
export default class DistributiviteSimpleDoubleReduction2e extends DistributiviteSimpleDoubleReduction {
}
