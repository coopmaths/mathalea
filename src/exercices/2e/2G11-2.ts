import ProblemesThales from '../3e/3G20-1'
export const titre = 'Résoudre des problèmes utilisant le théorème de Thalès'
export const interactifReady = false
export const uuid = 'eab10'
export const refs = {
  'fr-fr': ['2G11-2'],
  'fr-ch': []
}
export default class ProblemesThales2nde extends ProblemesThales {
}
