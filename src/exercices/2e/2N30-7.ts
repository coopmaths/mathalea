import ProblemesAdditifsFractionsBis from '../4e/4C25-0'
export const titre = 'Résoudre des problèmes additifs et de comparaison sur les fractions'
export const interactifReady = false
export const uuid = 'd0fdc'
export const refs = {
  'fr-fr': ['2N30-7'],
  'fr-ch': []
}
export default class ProblemesAdditifsFractionsBis2nde extends ProblemesAdditifsFractionsBis {
}
