import FractionsIrreductibles from '../3e/3A11'
export const interactifReady = false
export const titre = 'Rendre irréductible une fraction'
export const uuid = 'c1561'
export const refs = {
  'fr-fr': ['2N30-6'],
  'fr-ch': []
}
export default class FractionsIrreductibles2nde extends FractionsIrreductibles {
}
