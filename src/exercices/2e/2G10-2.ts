import demonstrationsParallelogrammes from '../5e/5G42'
export const titre = 'Déterminer la nature de parallélogrammes'
export const interactifReady = false
export const uuid = '86a65'

export const refs = {
  'fr-fr': ['2G10-2'],
  'fr-ch': []
}
export default class DemonstrationsParallelogrammes2nde extends demonstrationsParallelogrammes {
}
