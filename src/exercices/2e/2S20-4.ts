import CalculerEtendues from '../3e/3S15'
export const titre = 'Calculer des étendues'
export const interactifReady = true
export const interactifType = 'mathLive'
export const uuid = '55d00'
export const refs = {
  'fr-fr': ['2S20-4'],
  'fr-ch': []
}
export default class CalculerEtendues2nde extends CalculerEtendues {
}
