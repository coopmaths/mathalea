export const dictionnaireSTI2D = {

  sti2d_2024_06_metropole_maths: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Intégration']
  },
  sti2d_2024_06_metropole_mq1: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2024_06_metropole_mq2: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2024_06_metropole_mq3: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2024_06_metropole_mq4: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Intégration']
  },
  sti2d_2024_06_metropole_pcm: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Dérivation', 'Équation', 'Logarithme népérien']
  },

  sti2d_2021_03_sujet0v1_maths: {
    annee: '2021',
    lieu: 'sujet0v1',
    mois: '03',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Trigonométrie', 'Intégration', 'Équation', 'Exponentielle']
  },
  sti2d_2021_03_sujet0v1_mq1: {
    annee: '2021',
    lieu: 'sujet0v1',
    mois: '03',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2021_03_sujet0v1_mq2: {
    annee: '2021',
    lieu: 'sujet0v1',
    mois: '03',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Trigonométrie']
  },
  sti2d_2021_03_sujet0v1_mq3: {
    annee: '2021',
    lieu: 'sujet0v1',
    mois: '03',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Intégration']
  },
  sti2d_2021_03_sujet0v1_mq4: {
    annee: '2021',
    lieu: 'sujet0v1',
    mois: '03',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Équation', 'Exponentielle']
  },
  sti2d_2021_03_sujet0v2_maths: {
    annee: '2021',
    lieu: 'sujet0v2',
    mois: '03',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes', 'Géométrie repérée', 'Équations différentielles', 'Logarithme népérien', 'Dérivation', 'Variations', 'Limites', 'Python']
  },
  sti2d_2021_03_sujet0v2_mq1: {
    annee: '2021',
    lieu: 'sujet0v2',
    mois: '03',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2021_03_sujet0v2_mq2: {
    annee: '2021',
    lieu: 'sujet0v2',
    mois: '03',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2021_03_sujet0v2_mq3: {
    annee: '2021',
    lieu: 'sujet0v2',
    mois: '03',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes', 'Géométrie repérée']
  },
  sti2d_2021_03_sujet0v2_mq4: {
    annee: '2021',
    lieu: 'sujet0v2',
    mois: '03',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2021_03_sujet0v2_mq5: {
    annee: '2021',
    lieu: 'sujet0v2',
    mois: '03',
    numeroInitial: 'mq5',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Dérivation', 'Variations']
  },
  sti2d_2021_03_sujet0v2_mq6: {
    annee: '2021',
    lieu: 'sujet0v2',
    mois: '03',
    numeroInitial: 'mq6',
    typeExercice: 'sti2d',
    tags: ['Limites', 'Python']
  },
  sti2d_2021_03_sujet0v2_pcm: {
    annee: '2021',
    lieu: 'sujet0v2',
    mois: '03',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation', 'Intégration']
  },
  sti2d_2021_06_metropole_j1_maths: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J1 - maths',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Tangente', 'Équation', 'Exponentielle', 'Dérivation', 'Variations', 'Limites', 'Équations différentielles', 'Intégration', 'Trigonométrie']
  },
  sti2d_2021_06_metropole_j1_mq1: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J1 - mq1',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Tangente', 'Équation']
  },
  sti2d_2021_06_metropole_j1_mq2: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J1 - mq2',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation']
  },
  sti2d_2021_06_metropole_j1_mq3: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J1 - mq3',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Dérivation', 'Variations', 'Limites']
  },
  sti2d_2021_06_metropole_j1_mq4: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J1 - mq4',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2021_06_metropole_j1_mq5: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J1 - mq5',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation', 'Intégration']
  },
  sti2d_2021_06_metropole_j1_mq6: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J1 - mq6',
    typeExercice: 'sti2d',
    tags: ['Trigonométrie']
  },
  sti2d_2021_06_metropole_j1_pcm: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J1 - pcm',
    typeExercice: 'sti2d',
    tags: ['Logarithme décimal']
  },
  sti2d_2021_06_metropole_j2_maths: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J2 - maths',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Dérivation', 'Tangente', 'Variations', 'Nombres complexes', 'Exponentielle', 'Géométrie repérée', 'Intégration', 'Trigonométrie']
  },
  sti2d_2021_06_metropole_j2_mq1: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J2 - mq1',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Dérivation', 'Tangente']
  },
  sti2d_2021_06_metropole_j2_mq2: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J2 - mq2',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Dérivation', 'Variations']
  },
  sti2d_2021_06_metropole_j2_mq3: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J2 - mq3',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation']
  },
  sti2d_2021_06_metropole_j2_mq4: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J2 - mq4',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes', 'Géométrie repérée']
  },
  sti2d_2021_06_metropole_j2_mq5: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J2 - mq5',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Géométrie repérée', 'Intégration']
  },
  sti2d_2021_06_metropole_j2_mq6: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J2 - mq6',
    typeExercice: 'sti2d',
    tags: ['Trigonométrie']
  },
  sti2d_2021_06_metropole_j2_pcm: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '06',
    numeroInitial: 'J2 - pcm',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Limites', 'Équations différentielles', 'Équation']
  },
  sti2d_2021_09_metropole_maths: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation', 'Variations', 'Logarithme népérien', 'Équation', 'Équations différentielles', 'Nombres complexes', 'Géométrie repérée']
  },
  sti2d_2021_09_metropole_mq1: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation', 'Variations']
  },
  sti2d_2021_09_metropole_mq2: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Équation']
  },
  sti2d_2021_09_metropole_mq3: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation']
  },
  sti2d_2021_09_metropole_mq4: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2021_09_metropole_mq5: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq5',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2021_09_metropole_mq6: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq6',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes', 'Géométrie repérée']
  },
  sti2d_2021_09_metropole_pcm: {
    annee: '2021',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Intégration']
  },

  sti2d_2022_05_etrangers_maths: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: '05',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Variations', 'Nombres complexes', 'Géométrie repérée', 'Logarithme népérien', 'Équation', 'Équations différentielles', 'Limites', 'Trigonométrie']
  },
  sti2d_2022_05_etrangers_mq1: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: '05',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Variations']
  },
  sti2d_2022_05_etrangers_mq2: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: '05',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes', 'Géométrie repérée']
  },
  sti2d_2022_05_etrangers_mq3: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: '05',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Équation']
  },
  sti2d_2022_05_etrangers_mq4: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: '05',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2022_05_etrangers_mq5: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: '05',
    numeroInitial: 'mq5',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Limites']
  },
  sti2d_2022_05_etrangers_mq6: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: '05',
    numeroInitial: 'mq6',
    typeExercice: 'sti2d',
    tags: ['Trigonométrie', 'Équation']
  },
  sti2d_2022_05_etrangers_pcm: {
    annee: '2022',
    lieu: 'Centres étrangers',
    mois: '05',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Limites', 'Exponentielle', 'Dérivation']
  },
  sti2d_2022_05_metropole_maths: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '05',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Nombres complexes', 'Équations différentielles', 'Exponentielle', 'Équation', 'Dérivation', 'Tangente', 'Variations']
  },
  sti2d_2022_05_metropole_mq1: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '05',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien']
  },
  sti2d_2022_05_metropole_mq2: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '05',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2022_05_metropole_mq3: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '05',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2022_05_metropole_mq4: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '05',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation', 'Dérivation', 'Tangente']
  },
  sti2d_2022_05_metropole_mq5: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '05',
    numeroInitial: 'mq5',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Dérivation', 'Variations']
  },
  sti2d_2022_05_metropole_mq6: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '05',
    numeroInitial: 'mq6',
    typeExercice: 'sti2d',
    tags: ['Trigonométrie']
  },
  sti2d_2022_05_metropole_pcm: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '05',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Limites', 'Équation']
  },
  sti2d_2022_05_polynesie_maths: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: '05',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Nombres complexes', 'Exponentielle', 'Équation', 'Variations', 'Logarithme népérien', 'Limites']
  },
  sti2d_2022_05_polynesie_mq1: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: '05',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2022_05_polynesie_mq2: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: '05',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2022_05_polynesie_mq3: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: '05',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation']
  },
  sti2d_2022_05_polynesie_mq4: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: '05',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Variations']
  },
  sti2d_2022_05_polynesie_mq5: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: '05',
    numeroInitial: 'mq5',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Équation']
  },
  sti2d_2022_05_polynesie_mq6: {
    annee: '2022',
    lieu: 'Polynésie',
    mois: '05',
    numeroInitial: 'mq6',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Limites']
  },
  sti2d_2022_09_metropole_maths: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Exponentielle', 'Équation', 'Équations différentielles', 'Nombres complexes', 'Trigonométrie']
  },
  sti2d_2022_09_metropole_mq1: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Exponentielle']
  },
  sti2d_2022_09_metropole_mq2: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Équation']
  },
  sti2d_2022_09_metropole_mq3: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2022_09_metropole_mq4: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2022_09_metropole_mq5: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq5',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation']
  },
  sti2d_2022_09_metropole_mq6: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq6',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Trigonométrie']
  },
  sti2d_2022_09_metropole_pcm: {
    annee: '2022',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation', 'Limites']
  },
  sti2d_2022_10_caledonie_maths: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: '10',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Exponentielle', 'Dérivation', 'Tangente', 'Variations', 'Limites', 'Python', 'Équation']
  },
  sti2d_2022_10_caledonie_mq1: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: '10',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2022_10_caledonie_mq2: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: '10',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation', 'Tangente']
  },
  sti2d_2022_10_caledonie_mq3: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: '10',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Variations']
  },
  sti2d_2022_10_caledonie_mq4: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: '10',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Limites']
  },
  sti2d_2022_10_caledonie_mq5: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: '10',
    numeroInitial: 'mq5',
    typeExercice: 'sti2d',
    tags: ['Python']
  },
  sti2d_2022_10_caledonie_mq6: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: '10',
    numeroInitial: 'mq6',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation']
  },
  sti2d_2022_10_caledonie_pcm: {
    annee: '2022',
    lieu: 'Nouvelle Calédonie',
    mois: '10',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Exponentielle', 'Équation']
  },

  sti2d_2023_03_metropole_maths: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '03',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation', 'Nombres complexes', 'Logarithme népérien', 'Équation']
  },
  sti2d_2023_03_metropole_mq1: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '03',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Exponentielle']
  },
  sti2d_2023_03_metropole_mq2: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '03',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation']
  },
  sti2d_2023_03_metropole_mq3: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '03',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2023_03_metropole_mq4: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '03',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Équation']
  },
  sti2d_2023_03_metropole_pcm: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '03',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Limites']
  },
  sti2d_2023_03_polynesie_maths: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: '03',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Limites', 'Dérivation', 'Variations', 'Nombres complexes']
  },
  sti2d_2023_03_polynesie_mq1: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: '03',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Limites', 'Dérivation', 'Variations']
  },
  sti2d_2023_03_polynesie_mq2: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: '03',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2023_03_polynesie_pcm: {
    annee: '2023',
    lieu: 'Polynésie',
    mois: '03',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Limites', 'Équation']
  },
  sti2d_2023_03_reunion_maths: {
    annee: '2023',
    lieu: 'reunion',
    mois: '03',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Nombres complexes']
  },
  sti2d_2023_03_reunion_mq1: {
    annee: '2023',
    lieu: 'reunion',
    mois: '03',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien']
  },
  sti2d_2023_03_reunion_mq2: {
    annee: '2023',
    lieu: 'reunion',
    mois: '03',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2023_03_reunion_pcm: {
    annee: '2023',
    lieu: 'reunion',
    mois: '03',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2023_06_mexique_maths: {
    annee: '2023',
    lieu: 'mexique',
    mois: '06',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Exponentielle', 'Dérivation', 'Nombres complexes', 'Équation']
  },
  sti2d_2023_06_mexique_mq1: {
    annee: '2023',
    lieu: 'mexique',
    mois: '06',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2023_06_mexique_mq2: {
    annee: '2023',
    lieu: 'mexique',
    mois: '06',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation']
  },
  sti2d_2023_06_mexique_mq3: {
    annee: '2023',
    lieu: 'mexique',
    mois: '06',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2023_06_mexique_mq4: {
    annee: '2023',
    lieu: 'mexique',
    mois: '06',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation']
  },
  sti2d_2023_06_mexique_pcm: {
    annee: '2023',
    lieu: 'mexique',
    mois: '06',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation', 'Tangente', 'Équation']
  },
  sti2d_2023_09_caledonie_maths: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: '09',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes', 'Exponentielle', 'Tangente', 'Variations', 'Équation']
  },
  sti2d_2023_09_caledonie_mq1: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: '09',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2023_09_caledonie_mq2: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: '09',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Tangente', 'Variations', 'Équation']
  },
  sti2d_2023_09_caledonie_pcm: {
    annee: '2023',
    lieu: 'Nouvelle Calédonie',
    mois: '09',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Exponentielle', 'Équation']
  },
  sti2d_2023_09_metropole_maths: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Logarithme décimal', 'Logarithme népérien', 'Limites', 'Nombres complexes', 'Géométrie repérée', 'Équations différentielles']
  },
  sti2d_2023_09_metropole_mq1: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Logarithme décimal', 'Logarithme népérien']
  },
  sti2d_2023_09_metropole_mq2: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Limites']
  },
  sti2d_2023_09_metropole_mq3: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes', 'Géométrie repérée']
  },
  sti2d_2023_09_metropole_mq4: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles']
  },
  sti2d_2023_09_metropole_pcm: {
    annee: '2023',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Équation', 'Dérivation']
  },

  sti2d_2024_06_polynesie_maths: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: '06',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes', 'Logarithme décimal']
  },
  sti2d_2024_06_polynesie_mq1: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: '06',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2024_06_polynesie_mq2: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: '06',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2024_06_polynesie_mq3: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: '06',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Logarithme décimal']
  },
  sti2d_2024_06_polynesie_mq4: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: '06',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2024_06_polynesie_pcm: {
    annee: '2024',
    lieu: 'Polynésie',
    mois: '06',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Équations différentielles', 'Limites', 'Exponentielle', 'Équation']
  },
  sti2d_2024_09_metropole_maths: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'maths',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien', 'Exponentielle', 'Dérivation', 'Nombres complexes']
  },
  sti2d_2024_09_metropole_mq1: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq1',
    typeExercice: 'sti2d',
    tags: ['Logarithme népérien']
  },
  sti2d_2024_09_metropole_mq2: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq2',
    typeExercice: 'sti2d',
    tags: ['Exponentielle', 'Dérivation']
  },
  sti2d_2024_09_metropole_mq3: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq3',
    typeExercice: 'sti2d',
    tags: ['Nombres complexes']
  },
  sti2d_2024_09_metropole_mq4: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'mq4',
    typeExercice: 'sti2d',
    tags: ['Intégration']
  },
  sti2d_2024_09_metropole_pcm: {
    annee: '2024',
    lieu: 'Métropole',
    mois: '09',
    numeroInitial: 'pcm',
    typeExercice: 'sti2d',
    tags: ['Logarithme décimal', 'Équation', 'Équations différentielles', 'Exponentielle']
  },

}
